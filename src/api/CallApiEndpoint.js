import axios from "axios";

const axiosInstance = axios.create(
    {
        headers: {
            "Content-type": "application/json"
        }
    }
);
const cancelTokenSource = axios.CancelToken.source();

export default function callApiEndpoint(endpoint, method, data, successCallback, failedCallback, alwaysCallback)
{
    axiosInstance({
        url: `${endpoint}`,
        method: method,
        data: (data)? data : {},
        responseType: "json",
        cancelToken: cancelTokenSource.token
    })
    .then( 
        (resp) => 
        {
            console.log("Response: ");
            console.log(resp);
            if (resp.data)
            {
                if (resp.data.status == "failed")
                {
                    throw new Error(resp.data.data.message);
                }
                successCallback(resp.data.data);
            }
        }
    )
    .catch(
        (error) =>
        {
            console.log("Axios Error: ");
            console.log(error);

            let errMsg = "";
            if (error) 
            {
                if (axios.isCancel(error))
                {
                    console.log("Request Cancelled. " + error.message);

                } else if (error.response && error.response.data)
                {
                    errMsg = (error.response.data.data != undefined)? 
                                error.response.data.data.message
                                :
                                error.response.data;
                } else 
                {
                    errMsg = error.message;
                }
            }

            failedCallback((error.response) ? error.response.data : errMsg);
        }
    )
    .then(
        () => {
            if (alwaysCallback) 
            {
                alwaysCallback();
            }
        }
    );
}