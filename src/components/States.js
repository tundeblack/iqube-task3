import React from "react";
import { hot } from "react-hot-loader";
import { connect } from "react-redux";
import StateDataRow from "./StateDataRow.js";

const component = (props) => 
{
    return (
        <section>
            <div className = "container-fluid confirm-payment-box my-4">
                <div className = "row confirm-payment-header-blue-background">
                    <div className = "col-md-4 t-white px-4 py-2">States</div>
                    <div className = "col-md-2 t-white px-4 py-2">Confirmed</div>
                    <div className = "col-md-2 t-white px-4 py-2">Admitted</div>
                    <div className = "col-md-2 t-white px-4 py-2">Discharged</div>
                    <div className = "col-md-2 t-white px-4 py-2">Death</div>
                </div>
                {
                    props.states.map(
                        (s, i) => 
                        {
                            let component = <StateDataRow
                                state = { s.state } 
                                confirmed = { s.confirmedCases }
                                admitted = { s.casesOnAdmission }
                                discharged = { s.discharged }
                                death = { s.death }
                            />;

                            return (
                                <React.Fragment key = { i }>
                                    { component }
                                    {
                                        (i < props.states.length - 1) ? <hr className = "my-2" /> : null
                                    }
                                </React.Fragment>
                            )
                        }
                    )
                }
            </div>
        </section>
    );
}

const States = connect(
    (state) => 
    {
        return {
            states: state.states
        }
    }
)(component);

export default hot(module)(States);