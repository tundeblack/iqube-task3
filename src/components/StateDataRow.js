import React from "react";
import { hot } from "react-hot-loader";


const StateDataRow = (props) => 
{
    return (
        <div className = "row">
            <div className = "col-md-4 t-purple-n px-4 py-2">{ props.state }</div>
            <div className = "col-md-2 t-purple-n px-4 py-2">{ props.confirmed }</div>
            <div className = "col-md-2 t-purple-n px-4 py-2">{ props.admitted }</div>
            <div className = "col-md-2 t-purple-n px-4 py-2">{ props.discharged }</div>
            <div className = "col-md-2 t-purple-n px-4 py-2">{ props.death }</div>
        </div>
    );
}

export default hot(module)(StateDataRow);