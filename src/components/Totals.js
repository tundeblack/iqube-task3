import React from "react";
import { hot } from "react-hot-loader";
import { connect } from "react-redux";


const component = (props) =>
{   
    return (
        <section>
            <div className = "container-fluid confirm-payment-box">
                <div className = "row confirm-payment-header-blue-background">
                    <div className = "col-12 t-white px-4 py-2">Totals</div>
                </div>
                <div className = "row p-4">
                    <div className = "col-md-9">
                        <p className = "t-purple">Total Samples Tested</p>
                        <p className = "t-purple">Total Confirmed Cases</p>
                        <p className = "t-purple">Total Active Cases</p>
                        <p className = "t-purple">Discharged</p>
                        <p className = "t-purple">Death</p>
                    </div>
                    <div className = "col-md-3">
                        <p className = "text-right t-purple-n">{ props.totals.totalSamplesTested }</p>
                        <p className = "text-right t-purple-n">{ props.totals.totalConfirmedCases }</p>
                        <p className = "text-right t-purple-n">{ props.totals.totalActiveCases }</p>
                        <p className = "text-right t-purple-n">{ props.totals.discharged }</p>
                        <p className = "text-right t-purple-n">{ props.totals.death }</p>
                    </div>
                </div>
            </div>
        </section>
    );
}

const Totals = connect(
    (state) => 
    {
        return {
            totals: state.totals
        };
    }
)(component);

export default hot(module)(Totals);