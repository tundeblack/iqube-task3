import React from "react";
import { hot } from "react-hot-loader";

const Busy = () => 
{
    return (
        <div className="text-center">
            <i className="fa fa-spinner fa-pulse fa-2x" style={{color:"rgb(15, 101, 135)"}}></i>
        </div>
    );
}

export default hot(module)(Busy);