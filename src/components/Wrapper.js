import React from "react";
import { hot } from "react-hot-loader";

const Wrapper = (props) => 
{
    return (
        <section>
            <div className="container p-md-0">
                {
                    props.children
                }        
            </div>
        </section>
    );
}

export default hot(module)(Wrapper);

