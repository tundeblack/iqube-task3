import React from "react";
import { hot } from "react-hot-loader";

function Anchor(props)
{
    const { type, linkTo, classes, onClickCallback, content, ...restProps } = props;
    return (
        <a 
            href = { linkTo }
            className = { classes }
            onClick = { (onClickCallback)? onClickCallback : () => {} }
            { ...restProps }
        >
            { content }
        </a>
    );
}

export default hot(module)(Anchor);