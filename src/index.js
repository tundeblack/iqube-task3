import React from "react";
import ReactDom from "react-dom";
import { Provider } from "react-redux";
import Task3App from "./Task3App.js";
import { reduxStore, persistor } from "./redux/Store.js";
import { PersistGate } from "redux-persist/integration/react";
import Busy from "./components/Busy.js";


ReactDom.render(
    <Provider store = { reduxStore }>
        <PersistGate loading={<Busy />} persistor={ persistor }>
            <Task3App />
        </PersistGate>
    </Provider>,
    document.getElementById("appRoot")
);