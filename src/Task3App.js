import React, { useEffect } from "react";
import { connect } from "react-redux";
import { hot } from "react-hot-loader";
import { 
    clearAppErrorMsgAction
} from "./redux/ActionCreators.js";
import Alert from "./components/Alert.js"
import Covid19Report from "./views/Covid19Report.js";


const app = (props) => 
{
    return (
        <section>
            {
                (props.appErrorMsg)?
                    <div className = "sticky-top" style={{width:"100%",position:"fixed",left:"0",top:"0",zIndex:"10000000"}}>
                        <div className="row justify-content-center">
                            <div className="col-md-5">
                                <Alert 
                                    type = "error"
                                    message = { props.appErrorMsg }
                                    ephemeral = { true }
                                    onDisappear = {
                                        () => 
                                        {
                                            props.dispatch(clearAppErrorMsgAction());
                                        }
                                    }
                                />
                            </div>
                        </div>
                    </div>
                :
                    null
            }

            <Covid19Report />
        </section>
    );
}

const Task3App = connect(
    (state) => 
    {
        return { 
            appErrorMsg: state.appErrorMessage
        };
    },
    (dispatch) => 
    {
        return { dispatch };
    }
)(app);

export default hot(module)(Task3App);