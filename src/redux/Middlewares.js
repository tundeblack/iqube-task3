import * as Actions from "../constants/Actions.js";


export const covidReportFetched = ({ getState, dispatch }) => 
{
    return (next) => 
    {
        return (action) => 
        {
            if (action.type == Actions.FETCH_REPORT_SUCCESSFUL)
            {
                dispatch({
                    type: Actions.SET_TOTALS_DATA,
                    payload: {
                        totalSamplesTested: action.payload.totalSamplesTested,
                        totalConfirmedCases: action.payload.totalConfirmedCases,
                        totalActiveCases: action.payload.totalActiveCases,
                        discharged: action.payload.discharged,
                        death: action.payload.death
                    }
                });
                dispatch({
                    type: Actions.SET_STATES_DATA,
                    payload: action.payload.states
                });
            }

            next(action);
        }
    }
}