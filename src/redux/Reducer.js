"use-strict";

import * as Actions from "../constants/Actions.js";

const initialState = {
    appErrorMsg: "",
    totals: {},
    states: [],
    fetchingReport: false
};

const reducer = (state = initialState, action) => 
{
    const objAssign = (newState) =>
    {
        return Object.assign({}, state, newState);
    }
    
    switch (action.type)
    {
        case Actions.SET_APP_ERROR:
            return objAssign({
                appErrorMsg: action.payload
            });
        case Actions.CLEAR_APP_ERROR:
            return objAssign({
                appErrorMsg: ""
            });

        case Actions.SET_TOTALS_DATA:
            return objAssign(
                {
                    totals: action.payload
                }
            );

        case Actions.SET_STATES_DATA:
            return objAssign({
                states: action.payload
            });
        case Actions.FETCHING_REPORT:
            return objAssign({
                fetchingReport: action.payload
            });

        default:
            return state;
    }
}

export default reducer;