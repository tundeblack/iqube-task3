import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducer from "./Reducer.js";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import {
    covidReportFetched
} from "./Middlewares.js";


const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE || compose;

const persistCfg = {
    key: "covid19Report",
    storage
};

const middleware = [
    thunk,
    covidReportFetched
];

const reduxStore = createStore(
    persistReducer(persistCfg, reducer),
    storeEnhancers(applyMiddleware(...middleware))
);

const persistor = persistStore(reduxStore);

export { reduxStore, persistor };