import callApiEndpoint from "../api/CallApiEndpoint.js";
import * as Action from "../constants/Actions.js";


export const fetchCovidReportAction = () => 
{
    return (dispatch, getState) => 
    {
        dispatch({ type: Action.FETCHING_REPORT, payload: true });

        return callApiEndpoint(
            "https://covidnigeria.herokuapp.com/api",
            "get",
            {},
            (resp) => 
            {
                dispatch({ type: Action.FETCHING_REPORT, payload: false });
                dispatch({ 
                    type: Action.FETCH_REPORT_SUCCESSFUL, 
                    payload: resp 
                });
            },
            (error) => 
            {
                dispatch({ type: Action.FETCHING_REPORT, payload: false });
                dispatch({ 
                    type: Action.FETCH_REPORT_ERRORED, 
                    payload: error 
                });
            }
        );
    }
}

export const clearAppErrorMsgAction = () => 
{
    return {
        type: Action.CLEAR_APP_ERROR
    };
}
