import React, { useEffect } from "react";
import { connect } from "react-redux";
import { hot } from "react-hot-loader";
import Button from "../components/Button.js";
import Busy from "../components/Busy.js";
import States from "../components/States.js";
import Totals from "../components/Totals.js";
import Wrapper from "../components/Wrapper.js";
import { fetchCovidReportAction } from "../redux/ActionCreators.js";


const component = (props) => 
{
    useEffect(
        () => 
        {
            props.dispatch(
                fetchCovidReportAction()
            );
        },
        []
    );

    return (
        <section>
            <Wrapper>
                <div className = "row justify-content-center">
                    <div className = "col-md-10">
                        
                        <div className = "row align-items-center">
                            <div className = "col-md-10">
                                <h3 className = "heading">Covid 19 Report</h3>
                            </div>
                            <div className = "col-md-2 text-right">
                                <Button 
                                    classes = "btn btn-light"
                                    content = { <i className = "fa fa-refresh"></i> }
                                    onClickCallback = {
                                        (e) => 
                                        {
                                            props.dispatch(
                                                fetchCovidReportAction()
                                            );
                                        }
                                    }
                                />
                            </div>
                        </div>
                        
                        {
                            (props.fetchingReport) ?
                                <Busy />
                            :
                                null
                        }

                        <Totals />
                        
                        <States />
                    </div>
                </div>
            </Wrapper>
        </section>
    );
}

const Covid19Report = connect(
    (state) => 
    {
        return {
            fetchingReport: state.fetchingReport
        }
    },
    (dispatch) => 
    {
        return { dispatch };
    }
)(component);
export default hot(module)(Covid19Report);